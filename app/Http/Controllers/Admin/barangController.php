<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;

class barangController extends Controller
{
    public function index()
    {
        $barang = Barang::all();
        return view('admin.barang.index',['barang' => $barang]);
    }
}
