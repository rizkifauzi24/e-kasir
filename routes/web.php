<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::prefix('admin')->group(function () {
//group route with middleware "auth"
    Route::group(['middleware' => 'auth'], function() {
        //OBAT
        Route::get('/obat', [ObatController::class,'index'])->name('admin.obat.index');

        //SIGNA
        Route::get('/signa', [SignaController::class,'index'])->name('admin.signa.index');

        //NON RACIKAN
        Route::get('/nonracikan',[nonRacikanController::class,'index'])->name('admin.nonracikan.index');
        Route::get('/nonracikan/tambah',[nonRacikanController::class,'tambah'])->name('admin.nonracikan.tambah');
        Route::post('/nonracikan/simpan',[nonRacikanController::class,'simpan'])->name('admin.nonracikan.simpan');
        Route::delete('/nonracikan/hapus/{id}',[nonRacikanController::class,'hapus'])->name('admin.nonracikan.hapus');
        Route::get('/nonracikan/edit/{id}',[nonRacikanController::class,'edit'])->name('admin.nonracikan.edit');
        Route::post('/nonracikan/{id}',[nonRacikanController::class,'update'])->name('admin.nonracikan.update');
        Route::get('/nonracikan/printpdf',[nonRacikanController::class,'printpdf'])->name('admin.nonracikan.printpdf');

        //BARANG
        Route::get('/catatbarang',[barangController::class,'index'])->name('admin.catatbarang.index');
    });
});