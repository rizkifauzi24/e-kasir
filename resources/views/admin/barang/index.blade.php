@extends('layouts.app', ['title' => 'Transaksi'])

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid mb-5">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 shadow">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold"><i class="fas fa-folder"></i> TRANSAKSI</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.nonracikan.simpan') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                         <div class="transaksi">   
                            <button class="btn btn-primary btn-sm mr-1 add-barang float-right" type="button"><i class="fas fa-plus-circle"></i> Tambah Barang</button>
                            <br>
                            <div class="form-group">   
                                <label for="exampleFormControlSelect2">Barang</label>
                                <select required class="js-example-basic-single" data-width="100%" name="nama_barang[]" >
                                    <option value="">--Pilih Barang--</option>
                                    @foreach($barang as $barangs)
                                    <option data-harga="{{ $barangs->harga_satuan }}"  value="{{ $barang[0]->id }}">{{ $barangs->nama_barang }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Qty</label>
                                <input type="number" name="qty" value="{{ old('name') }}" id="qty" onkeyup="myFunction()" placeholder="Masukkan Qty" class="form-control @error('name') is-invalid @enderror">

                                @error('name')
                                <div class="invalid-feedback" style="display: block">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <br>
                        <div id="extra-barang">
                        </div>    
                        <br>
                        <button class="btn btn-primary mr-1 btn-submit" type="submit"><i class="fa fa-paper-plane"></i> SIMPAN</button>
                        <button class="btn btn-warning btn-reset" type="reset"><i class="fa fa-redo"></i> RESET</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

@endsection
@push('js')
    <script>
    const add = document.querySelectorAll(".transaksi .add-barang")
    add.forEach(function(e){
        e.addEventListener('click',function(){
            let element = this.parentElement
            // console.log(element);
            let newElement = document.createElement('div')
                newElement.classList.add('transaksi')
                newElement.innerHTML = `<button class="btn btn-danger btn-sm mr-1 remove-barang float-right" type="button"><i class="fas fa-plus-circle"></i> Remove Barang</button>`
                document.getElementById('extra-barang').appendChild(newElement)
            
            let form1 = document.createElement('div')
                form1.classList.add('form-group')
                form1.classList.add('mb-3')
                form1.innerHTML = `<label for="exampleFormControlSelect2">Barang</label>
                                <select required class="custom-select" name="nama_barang[]">
                                    <option value="">--Pilih Barang--</option>
                                    @foreach($barang as $barangs)
                                    <option data-harga="{{ $barangs->harga_satuan }}">{{ $barangs->nama_barang }}</option>
                                    @endforeach
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button">Button</button>
                                    </div>
                                </select>`
                document.getElementById('extra-barang').appendChild(form1)

            let form2 = document.createElement('div')
                form2.classList.add('form-group')
                form2.innerHTML = `<label>Qty</label>
                                <input type="number" name="qty" value="{{ old('name') }}" id="qty" onkeyup="myFunction()" placeholder="Masukkan Qty" class="form-control @error('name') is-invalid @enderror">

                                @error('name')
                                <div class="invalid-feedback" style="display: block">
                                    {{ $message }}
                                </div>
                                @enderror`
                document.getElementById('extra-barang').appendChild(form2)

            document.querySelector('form').querySelectorAll('.remove-barang ').forEach(function(remove){
                remove.addEventListener('click'.function(elmClick){
                    elmClick.target.form1.remove()
                })
            })
        })
    })
    </script>
@endpush