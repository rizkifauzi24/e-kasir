<?php

namespace Database\Seeders;

use App\Models\Barang;
use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Barang::create([
            'nama_barang' => 'Sabun batang',
            'harga_satuan' => '3000',
        ]);

        Barang::create([
            'nama_barang' => 'Mi instan',
            'harga_satuan' => '2000',
        ]);

        Barang::create([
            'nama_barang' => 'Pensil',
            'harga_satuan' => '1000',
        ]);

        Barang::create([
            'nama_barang' => 'Kopi sachet',
            'harga_satuan' => '1500',
        ]);

        Barang::create([
            'nama_barang' => 'Air minum galon',
            'harga_satuan' => '20000',
        ]);
    }
}
